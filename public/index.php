<?php

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpKernel;
use Symfony\Component\Routing;

require_once(__DIR__ . '/../vendor/autoload.php');

$session = new Session();
$session->start();
$request = Request::createFromGlobals();
$request->setSession($session);

$routes = include(__DIR__ . '/../src/routes.php');

$context = new Routing\RequestContext();
$context->fromRequest($request);
$matcher = new Routing\Matcher\UrlMatcher($routes, $context);

$container = include(__DIR__ . '/../src/container.php');

$controllerResolver = new HttpKernel\Controller\ControllerResolver();
$argumentResolver = new HttpKernel\Controller\ArgumentResolver();

try {
    $request->attributes->add(array_merge(['container' => $container], $matcher->match($request->getPathInfo())));
    $controller = $controllerResolver->getController($request);
    $arguments = $argumentResolver->getArguments($request, $controller);
    $response = call_user_func_array($controller, $arguments);
} catch (Routing\Exception\ResourceNotFoundException $exception) {
    $response = new Response('Not Found', 404);
} catch (Exception $exception) {
    $response = new Response('An error occurred' . $exception->getMessage(), 500);
}

$response->send();
