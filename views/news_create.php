<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/news">Список новостей</a></li>
    <?php if($action == 'edit'):?>
        <li class="breadcrumb-item active" aria-current="page">Редактирование</li>
    <?php elseif($action == 'create'):?>
        <li class="breadcrumb-item active" aria-current="page">Добавление</li>
    <?php endif;?>
  </ol>
</nav>

<form method="post">
  <?php if(!empty($error)): ?>
    <div class="alert alert-danger" role="alert">
      Оба поля обязательны для добавления
    </div>
  <?php endif; ?>
  <div class="form-group">
    <label for="news_title">Заголовок</label>
    <input type="text" class="form-control" name="news_title" id="news_title" placeholder="Заголовок" value="<?=htmlentities($title) ?? ''?>">
  </div>
  <div class="form-group">
    <label for="news_content">Описание</label>
    <textarea class="form-control" name="news_content" id="news_content" placeholder="Описание" rows="8" cols="80"><?=htmlentities($content) ?? ''?></textarea>
  </div>
  <?php if($action == 'edit'):?>
      <button type="submit" class="btn btn-primary">Сохранить</button>
      <a href="/news/<?=$id?>/delete" class="btn btn-danger">Удалить</a>
  <?php elseif($action == 'create'):?>
      <button type="submit" class="btn btn-primary">Добавить</button>
  <?php endif;?>
</form>
