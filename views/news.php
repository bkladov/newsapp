<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item active" aria-current="page">Список новостей</li>
  </ol>
</nav>

<a href="/news/create" class="btn btn-primary">Добавить</a>

<div style="padding-top: 10px;">
<?php foreach ($news as $item):?>
    <a href="/news/<?=$item['id']?>"><h3><?=htmlentities($item['title'])?> </h3></a>
    <small><?=htmlentities($item['created_at'])?></small>
<?php endforeach; ?>
</div>
