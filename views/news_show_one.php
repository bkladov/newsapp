<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="/news">Список новостей</a></li>
    <li class="breadcrumb-item active" aria-current="page"><?=htmlentities($title)?></li>
  </ol>
</nav>

<h1><?=htmlentities($title)?></h1>
<small><?=htmlentities($created_at)?></small>
<p><?=htmlentities($content)?></p>

<a href="/news/<?=$id?>/edit" class="btn btn-primary">Редактировать</a>
<a href="/news/<?=$id?>/delete" class="btn btn-danger">Удалить</a>
