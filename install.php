<?php

copy(__DIR__ . '/config/database.php.example', __DIR__ . '/config/database.php');

require_once(__DIR__ . '/vendor/autoload.php');

$dbConfig = include(__DIR__ . '/config/database.php');

$db = new App\Lib\Db($dbConfig['dsn'], $dbConfig['user'], $dbConfig['pass']);


$stmt = $db->getPdo()->prepare("
CREATE TABLE IF NOT EXISTS `news` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(45) CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL,
  `content` TEXT CHARACTER SET 'utf8' COLLATE 'utf8_general_ci' NULL,
  `created_at` DATETIME NULL,
  `edited_at` DATETIME NULL,
  PRIMARY KEY (`id`));
");

$stmt->execute();
