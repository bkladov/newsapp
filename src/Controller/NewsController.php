<?php

namespace App\Controller;

use App\Lib\NewsStorage;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class NewsController
{
    public function indexAction(Request $request, $container)
    {
        $news = $container->get('news_storage')->fetchAllDesc();
        $viewManager = $container->get('view');
        $content = $viewManager->make('news.php', ['news' => $news]);
        $layout = $viewManager->make('layout.php', ['content' => $content]);

        return new Response($layout);
    }

    public function showOneAction(Request $request, $id, $container)
    {
        $item = $container->get('news_storage')->findById($id);
        $viewManager = $container->get('view');
        $content = $viewManager->make('news_show_one.php', [
            'id' => $id,
            'title' => $item['title'],
            'content' => $item['content'],
            'created_at' => $item['created_at'],
        ]);
        $layout = $viewManager->make('layout.php', ['content' => $content]);

        return new Response($layout);
    }

    public function createAction(Request $request, $container)
    {
        $session = $request->getSession();
        if($request->isMethod('POST')) {
            $title = trim($request->request->get('news_title'));
            $content = trim($request->request->get('news_content'));
            try {
                $container->get('news_storage')->create($title, $content);

                return new RedirectResponse('/news', 302);
            } catch (\Exception $e) {
                $session->getFlashBag()->add('error', true);
                $session->getFlashBag()->add('news_title', $title);
                $session->getFlashBag()->add('news_content', $content);

                return new RedirectResponse('/news/create', 302);
            }
        }
        $viewManager = $container->get('view');
        $content = $viewManager->make('news_create.php', [
            'title' => $session->getFlashBag()->get('news_title')[0] ?? '',
            'content' => $session->getFlashBag()->get('news_content')[0] ?? '',
            'error' => $session->getFlashBag()->get('error')[0] ?? false,
            'action' => 'create',
        ]);
        $layout = $viewManager->make('layout.php', ['content' => $content]);

        return new Response($layout);
    }

    public function editAction(Request $request, $id, $container)
    {
        $item = $container->get('news_storage')->findById($id);

        if(empty($item)) return new RedirectResponse('/news', 302);

        $session = $request->getSession();
        if($request->isMethod('POST')) {
            $title = trim($request->request->get('news_title'));
            $content = trim($request->request->get('news_content'));
            try {
                $container->get('news_storage')->edit($id, $title, $content);

                return new RedirectResponse("/news/$id", 302);
            } catch (\Exception $e) {
                $session->getFlashBag()->add('error', true);
                $session->getFlashBag()->add('news_title', $title);
                $session->getFlashBag()->add('news_content', $content);

                return new RedirectResponse("/news/$id/edit", 302);
            }
        }

        $viewManager = $container->get('view');
        $content = $viewManager->make('news_create.php', [
            'id' => $id,
            'title' => $session->getFlashBag()->get('news_title')[0] ?? $item['title'],
            'content' => $session->getFlashBag()->get('news_content')[0] ?? $item['content'],
            'action' => 'edit',
            'error' => $session->getFlashBag()->get('error')[0] ?? false,
        ]);
        $layout = $viewManager->make('layout.php', ['content' => $content]);

        return new Response($layout);
    }

    public function deleteAction(Request $request, $id, $container)
    {
        $container->get('news_storage')->delete($id);
        return new RedirectResponse('/news', 302);
    }
}
