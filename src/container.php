<?php

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

$dbConfig = include(__DIR__ . '/../config/database.php');
$containerBuilder = new ContainerBuilder();
$containerBuilder->register('db', App\Lib\Db::class)
    ->setArguments([$dbConfig['dsn'], $dbConfig['user'], $dbConfig['pass']]);

$containerBuilder->register('news_storage', App\Lib\NewsStorage::class)
    ->setArguments([new Reference('db')]);

$containerBuilder->register('view', App\Lib\View::class)
    ->setArguments([__DIR__ . '/../views/']);

return $containerBuilder;
