<?php

namespace App\Lib;

interface DbConnectionInterface
{
    public function getPdo(): \PDO;
}
