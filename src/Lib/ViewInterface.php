<?php

namespace App\Lib;

interface ViewInterface
{
    public function make(string $viewPath, array $params = []): string;
}
