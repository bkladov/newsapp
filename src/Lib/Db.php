<?php

namespace App\Lib;

class Db implements DbConnectionInterface
{
    private $db;

    public function __construct(string $dsn, string $user, string $pass)
    {
        try {
            $this->db = new \PDO($dsn, $user, $pass);
        } catch (PDOException $e) {
            print $e->getMessage();
            die();
        }
    }

    public function getPdo(): \PDO
    {
        if($this->db === null) {
            throw new \Exception('Connection not set');
        }
        return $this->db;
    }
}
