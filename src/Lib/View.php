<?php

namespace App\Lib;

class View implements ViewInterface
{
    private $viewsDir;

    public function __construct(string $viewsDir)
    {
        $this->viewsDir = $viewsDir;
    }

    public function make(string $viewPath, array $params = []): string
    {
        ob_start();
        extract($params);
        include $this->viewsDir . $viewPath;
        return ob_get_clean();
    }
}
