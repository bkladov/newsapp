<?php

namespace App\Lib;

class NewsStorage
{
    private $db;

    public function __construct(\App\Lib\DbConnectionInterface $db)
    {
        $this->db = $db;
    }

    public function create(string $title, string $content)
    {
        if(empty($title) || empty($content)) {
            throw new \Exception("Error while create news", 1);
        }

        $pdo = $this->db->getPdo();
        $stmt = $pdo->prepare('INSERT INTO news (title, content, created_at) VALUES (:title, :content, :created_at)');
        $stmt->execute([
            'title' => $title,
            'content' => $content,
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        return $pdo->lastInsertId();
    }

    public function edit(int $id, string $title, string $content)
    {
        if(empty($title) || empty($content)) {
            throw new \Exception("Error while edit news", 1);
        }

        $pdo = $this->db->getPdo();
        $stmt = $pdo->prepare('UPDATE news SET title=:title, content=:content, edited_at=:edited_at WHERE id=:id');
        $stmt->execute([
            'id' => $id,
            'title' => $title,
            'content' => $content,
            'edited_at' => date('Y-m-d H:i:s'),
        ]);
    }

    public function delete(int $id)
    {
        $pdo = $this->db->getPdo();
        $stmt = $pdo->prepare('DELETE FROM news WHERE id=:id');
        $stmt->execute([
            'id' => $id,
        ]);
    }

    public function fetchAllDesc()
    {
        $pdo = $this->db->getPdo();
        $stmt = $pdo->prepare('SELECT * FROM news ORDER BY id DESC');
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_ASSOC);
    }

    public function findById(int $id)
    {
        if (empty($id)) return false;

        $pdo = $this->db->getPdo();
        $stmt = $pdo->prepare('SELECT * FROM news WHERE id=:id');
        $stmt->execute([
            'id' => $id,
        ]);

        return $stmt->fetch(\PDO::FETCH_ASSOC);
    }
}
