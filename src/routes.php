<?php

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

$routes = new RouteCollection();

$routes->add('main_page', new Route('/', [
    '_controller' => function() {
        return new RedirectResponse('/news');
    }
], [], [], '', [], ['GET']));

$routes->add('news_list', new Route('/news', [
    '_controller' => 'App\Controller\NewsController::indexAction'
], [], [], '', [], ['GET']));

$routes->add('news_create', new Route('/news/create', [
    '_controller' => 'App\Controller\NewsController::createAction'
], [], [], '', [], ['GET', 'POST']));

$routes->add('news_show_one', new Route('/news/{id}', [
    '_controller' => 'App\Controller\NewsController::showOneAction'
], [], [], '', [], ['GET']));

$routes->add('news_edit', new Route('/news/{id}/edit', [
    '_controller' => 'App\Controller\NewsController::editAction'
], [], [], '', [], ['GET', 'POST']));

$routes->add('news_delete', new Route('/news/{id}/delete', [
    '_controller' => 'App\Controller\NewsController::deleteAction'
], [], [], '', [], ['GET']));

return $routes;
